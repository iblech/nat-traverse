# nat-traverse: NAT gateway traversal utility

nat-traverse establishes direct connections between nodes which are behind NAT
gateways, i.e. hosts which do not have public IP addresses. This is done using
an UDP NAT traversal technique. Additionally, it's possible to setup a small
VPN by using pppd on top of nat-traverse.

nat-traverse does not need an external server on the Internet, and it isn't
necessary to reconfigure the involved NAT gateways, either. nat-traverse works
out-of-the-box.

[Documentation hosted elsewhere.](https://www.speicherleck.de/iblech/nat-traverse/)

The first version of nat-traverse was written in ancient times (2005). Somewhat
surprisingly, it still has its merits.

You can (and are invited to) use, redistribute and modify nat-traverse under
the terms of the GNU General Public License (GPL), version 3 or (at your
option) any later version published by the Free Software Foundation.
