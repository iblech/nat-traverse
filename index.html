<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<title>nat-traverse -- NAT gateway traversal utility</title>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="author" content="Ingo Blechschmidt &lt;iblech@speicherleck.de&gt;" />

<style type="text/css">
  body {
    background-color: white;
    margin:           0;

    font-family: sans-serif;
    line-height: 1.3em;
    font-size:   95%;
  }

  h1 {
    /* background-color: #008; */
    background-color: #313072;
    color:            white;
    padding:          10px;
    letter-spacing:   1px;
  }

  p, dl, pre, h2 {
    margin-left:      20px;
    width:            80%;
  }

  code { background-color: #eee; padding: 1px; }

  dt+dd p { margin-top: 0; }
</style>

</head>
<body>

<h1 id="name">Name</h1>

<p>nat-traverse &ndash; NAT gateway traversal utility</p>

<h1 id="synopsis">Synopsis</h1>

<p>To create a simple text-only tunnel, use the commands</p>

<pre><!--
-->user@left  $ nat-traverse 40000:natgw-of-right:40001
user@right $ nat-traverse 40001:natgw-of-left:40000<!--
--></pre>

<p>where <code>40000</code> is an unused UDP port on <code>left</code> and
<code>40001</code> is an unused UDP port on <code>right</code>.</p>

<h1 id="description">Description</h1>

<div style="float: right; width: 40%; margin: 0 0 1em 1em; background-color: #eee">
  <a href="nat-traverse.png">
    <img
      src="nat-traverse.png"
      style="border: 0; width: 100%"
      alt="Screenshot of nat-traverse in action"
    />
    Screenshot of nat-traverse in action
  </a>
</div>

<p>nat-traverse establishes connections between nodes which are behind NAT
gateways, i.e. hosts which do <em>not</em> have public IP addresses.
Additionally, you can <a href="#vpn-ppp">setup a small VPN</a> by using pppd on
top of nat-traverse. nat-traverse does <em>not</em> need an external server on
the Internet, and it isn't necessary to reconfigure the involved NAT gateways,
either. <em>nat-traverse works out-of-the-box.</em></p>

<div style="float: right; clear: both; width: 40%; margin: 0 0 1em 1em; background-color: #eee">
  <a href="nat-traverse-net.png">
    <img
      src="nat-traverse-net.png"
      style="border: 0; width: 100%"
      alt="Network diagram"
    />
    Example network architecture nat-traversal can work with
  </a>
</div>

<p>See <a href="#technique">below</a> for how this is achieved.</p>

<p>In other words: nat-traverse is a bit like <a
href="http://freecode.com/projects/harm">Harm</a>, but doesn't have Harm's
limitation that one peer has to have a public IP address.</p>

<p>Limitation: nat-traverse does not work with gateways which change the port
numbers. This is a fundamental problem of nat-traverse's design, as the changed
port numbers are (in general) not predictable.</p>

<h1 style="clear: both">Download</h1>

<p>nat-traverse is a <a href="https://dev.perl.org/perl5/">Perl 5</a> program using
only core modules, so neither compilation nor installation of Perl modules is
necessary. You do need a copy of Perl (&gt;= 5.6.1).</p>

<p>Simply grab <a
href="nat-traverse-0.7.pl"><code>nat-traverse-0.7.pl</code></a>, rename it to
<code>nat-traverse</code>, and flag it executable by using <code>chmod</code>.
Alternatively, you can download nat-traverse packed in a <a
href="nat-traverse-0.7.tar.bz2">bz2-compressed tarball</a>.</p>

<h1 id="options">Options</h1>

<dl>
  <dt><code><em>local_port</em>:<em>peer</em>:<em>remote_port</em></code> (required)</dt>
  <dd>
    <p>Sets the local port to use and the remote address to connect to.</p>

    <p>Note that you have to give the IP address or hostname of the <em>NAT
    gateway</em> of the host you want to connect to, as the target host doesn't
    have a public IP address.</p>
  </dd>

  <dt><code>--cmd=&quot;<em>pppd...</em>&quot;</code></dt>
  <dd>
    <p>Runs the specified command after establishing the connection.</p>

    <p>The command will be run with its STDIN and STDOUT bound to the socket,
    i.e. everything the command writes to STDOUT will be forwarded to the
    peer.</p>

    <p>If no command is specified, nat-traverse will relay input from STDIN to the
    peer and vice versa, i.e. nat-traverse degrades to netcat.</p>
  </dd>

  <dt><code>--window=<em>10</em></code></dt>
  <dd>
    <p>Sets the number of initial garbage packets to send. The default, 10,
    should work with most firewalls.</p>
  </dd>

  <dt><code>--timeout=<em>10</em></code></dt>
  <dd>
    <p>Sets the maximum number of seconds to wait for an acknowledgement by the
    peer.</p>
  </dd>

  <dt><code>--quit-after-connect</code></dt>
  <dd>
    <p>Quits nat-traverse after the tunnel has been established
    successfully.</p>

    <p>nat-traverse returns a non-<code>0</code> statuscode to indicate that
    it wasn't able to establish a tunnel.</p>

    <p><code>--quit-after-connect</code> is useful if you want another program
    to use the tunnel. For example, you could configure OpenVPN to use the the
    same ports as nat-traverse -- thus OpenVPN would be able to cross NAT
    gateways.</p>
  </dd>

  <dt><code>--version</code>, <code>--help</code></dt>
</dl>

<h1 id="technique">Technique</h1>

<p>nat-traverse establishes connections between hosts behind NAT gateways without
need for reconfiguration of the involved NAT gateways.</p>

<ol>
  <li>
    <p>Firstly, nat-traverse on host <code>left</code> sends garbage UDP packets to
    the NAT gateway of <code>right</code>. These packets are, of course,
    discarded by the firewall.</p>
  </li>
  <li>
    <p>Then <code>right</code>'s nat-traverse sends garbage UDP packets to the NAT
    gateway of <code>left</code>. These packets are <em>not</em> discarded, as
    <code>left</code>'s NAT gateway thinks these packets are replies to the
    packets sent in step 1!</p>
  </li>
  <li>
    <p><code>left</code>'s nat-traverse continues to send garbage packets to
    <code>right</code>'s NAT gateway. These packets are now not dropped
    either, as the NAT gateway thinks the packets are replies to the packets
    sent in step 2.</p>
  </li>
  <li>
    <p>Finally, both hosts send an acknowledgement packet to signal readiness.
    When these packets are received, the connection is established and nat-traverse
    can either relay STDIN/STDOUT to the socket or execute a program.</p>
  </li>
</ol>

<h1 id="examples">Examples</h1>

<h2 id="vpn-ppp">Setup of a small VPN with PPP</h2>

<p>It's easy to setup a small VPN (Virtual Private Network) by using the
Point-to-Point Protocol Daemon, <code>pppd</code>:</p>

<pre><!--
-->  root@left # nat-traverse \
      --cmd=&quot;pppd updetach noauth passive notty \
             ipparam vpn 10.0.0.1:10.0.0.2&quot;
      40000:natgw-of-right:40001
  root@right # nat-traverse \
      --cmd=&quot;pppd nodetach notty noauth&quot;
      40001:natgw-of-left:40000<!--
--></pre>

<div style="float: right; width: 40%; margin: 0 0 1em 1em; background-color: #eee">
  <a href="nat-traverse-vpn.png">
    <img
      src="nat-traverse-vpn.png"
      style="border: 0; width: 100%"
      alt="pppd running on top of nat-traverse"
    />
    pppd running on top of nat-traverse
  </a>
</div>

<p><code>pppd</code> creates a new interface, typically <code>ppp0</code>.
Using this interface, you can ping <code>10.0.0.1</code> or
<code>10.0.0.2</code>. As you can see, <code>pppd</code> upgrades the data-only
tunnel nat-traverse provides to a full IP tunnel. Thus you can establish
reliable TCP connections over the tunnel, even though nat-traverse uses UDP!
Furthermore, you could even add IPv6 addresses to <code>ppp0</code> by running
<code>ip -6 addr add...</code>!</p>

<p>Note though that although this VPN <em>is</em> arguably a private network, it is
<em>not</em> secured in any way. You may want to use SSH to encrypt the
connection.</p>

<h2 id="portfw">Port Forwarding with netcat</h2>

<p>You can use <code>netcat</code> to forward one of your local UDP or TCP
ports to an arbitrary UDP or TCP port of the remote host, similar to <code>ssh
-L</code> or <code>ssh -R</code>:</p>

<pre><!--
-->  user@left  $ nat-traverse 10001:natgw-of-right:10002 \
      --cmd=&quot;nc -vl 20000&quot;
  user@right $ nat-traverse 10002:natgw-of-left:10001 \
      --cmd=&quot;nc -v localhost 22&quot;<!--
--></pre>

<p>As soon as the tunnel is established (using UDP ports <code>10001</code> and
<code>10002</code>), <code>left</code>'s TCP port <code>20000</code> is
forwarded to <code>right</code>'s SSH Daemon (TCP port <code>22</code>):</p>

<pre><!--
-->  user@some-other-host $ ssh -p 20000 user@left
  # Will connect to right's SSH daemon!<!--
--></pre>

<p>But do note that you lose the reliability of TCP in this example, as the
actual data is transported via UDP; so this is only a toy example. If you want
reliable streams, use PPP on top of nat-traverse, as described <a
href="#vpn-ppp">above</a>.</p>

<h2 id="vpn-openvpn">Setup of a VPN with OpenVPN</h2>

<p>You can use <a href="https://openvpn.net/">OpenVPN</a> over
nat-traverse if you want to have a <em>secure</em> VPN.</p>

<p>Using OpenVPN over nat-traverse requires only one change to OpenVPN's
configuration file, presuming that you don't want to use OpenVPN's multi-client
mode: You have to adjust the <code>port</code> and <code>lport</code> options
accordingly, for example:</p>

<pre><!--
-->  # Options to add to left's and right's OpenVPN config:
  port  60001
  lport 60001

  # Command to execute on left resp. right:
  root@left  # until \
                 nat-traverse --quit-after-connect 60001:right:60001 \
               do \
                 sleep 5 \
               done; \
               openvpn [...]
  root@right # until \
                 nat-traverse --quit-after-connect 60001:left:60001 \
               do \
                 sleep 5 \
               done; \
               openvpn [...]<!--
--></pre>

<p>The <code>until</code> loop ensures that OpenVPN will not be started before
nat-traverse was able to establish the connection. Michael Kugele
(<code>michael (at) kugele.net</code>) also reported a way to still be able to
use OpenVPN's multi-client mode with nat-traverse: As all instances of
nat-traverse have to use unique ports (because a connection is identified by
the source/destination port combination), you've to use redirection rules to
redirect the ports used by nat-traverse to the port the OpenVPN daemon listens
on:</p>

<pre><!--
-->  iptables -t nat -A PREROUTING -p udp \
    --dport $LPORT -j DNAT --to $HOST:$PORT
  iptables -t nat -A PREROUTING -p udp \
    --dport $PORT -j REDIRECT --to-port $LPORT<!--
--></pre>

<p>(<code>$LPORT</code> specifies the source port nat-traverse uses on the server
side, and <code>$HOST:$PORT</code> is the address of the OpenVPN server.)</p>

<h1 id="limitations" style="clear: both">Limitations</h1>

<p>Only IPv4 is supported, nat-traverse won't work with IPv6 addresses.
<a href="mailto:iblech@web.de">Drop me a note</a> if you do need IPv6
support.</p>

<p>nat-traverse does not work with gateways which change the port numbers. This
is a fundamental problem of nat-traverse's design, as the changed port numbers
are (in general) not predictable.</p>

<h1 id="see_also">See also</h1>

<dl>
  <dt><a href="https://www.ietf.org/rfc/rfc1631.txt">RFC 1631</a></dt>
  <dd><p>
    The IP Network Address Translator (NAT). K. Egevang, P. Francis.  May 1994.
    (Obsoleted by RFC3022) (Status: INFORMATIONAL)
  </p></dd>

  <dt><a href="https://www.ietf.org/rfc/rfc3022.txt">RFC 3022</a></dt>
  <dd><p>
    Traditional IP Network Address Translator (Traditional NAT). P.  Srisuresh,
    K. Egevang. January 2001.  (Obsoletes RFC1631) (Status: INFORMATIONAL)
  </p></dd>

  <dt><a href="https://www.ietf.org/rfc/rfc1661.txt">RFC 1661</a></dt>
  <dd><p>
    The Point-to-Point Protocol (PPP). W. Simpson, Ed.. July 1994.  (Obsoletes
    RFC1548) (Updated by RFC2153) (Also STD0051) (Status: STANDARD)
  </p></dd>

  <dt><a href="https://ppp.samba.org/">pppd</a></dt>
  <dd><p>
    Website of Paul's PPP Package (open source implementation of the
    Point-to-Point Protocol (PPP) on Linux and Solaris)
  </p></dd>

  <dt><a href="nat-traverse-talk.pdf">German talk about nat-traverse</a></dt>
  <dd><p>
     Dieser Vortrag zeigt, wie man einen Tunnel zwischen zwei Computern, die
     beide hinter NAT-Gateways sitzen, hinbekommt. Dazu wird ein neues Programm
     vorgestellt, welches sowohl einfache Tastendrücke an die Gegenseite
     weiterleiten, als auch beliebige Programme mit Verbindungen zur Gegenseite
     starten kann. Damit ist ein einfaches VPN schnell aufgebaut.
  </p></dd>
</dl>

<h1 id="changelog">Changelog</h1>

<dl>
  <dt>v0.7, 2017-10-28</dt>
  <dd><p>Fixed a minor syntactical issue which caused a warning on modern
  Perl and relicensed under GPL version 3 or later.</p></dd>

  <dt>v0.6, 2012-12-24</dt>
  <dd><p>Fixed the port forwarding example in the documentation, thanks
  to Marcin Zajączkowski.</p></dd>

  <dt>v0.5, 2012-02-12</dt>
  <dd><p>Adjusted the length of garbage and acknowledgements packets to make
  nat-traverse work under Windows and Android. Thanks to Jacobo de Pedro for
  finding this bug!</p></dd>

  <dt>v0.4, 2005-08-23</dt>
  <dd><p>New option <code>--quit-after-connect</code> to quit nat-traverse after
  establishing the tunnel.</p></dd>

  <dt>v0.3, 2005-06-29</dt>
  <dd><p>Made nat-traverse work with Perl 5.6.1 (previously Perl 5.8.0 was
  required).</p></dd>

  <dt>v0.2, 2005-06-26</dt>
  <dd><p>Fixed a race condition which caused the two nat-traverse instances to
  sometimes miss each other.</p></dd>

  <dt>v0.1, 2005-06-25</dt>
  <dd><p>Initial release.</p></dd>
</dl>

<h1 id="author">Author</h1>

<p>Copyright (C) 2005, 2012, 2017 Ingo Blechschmidt, <code>&lt;<a
href="mailto:iblech@speicherleck.de">iblech@speicherleck.de</a>&gt;</code>.</p>

<p>The source code repository is hosted at <a
href="https://gitlab.com/iblech/nat-traverse">GitLab</a>.</p>

<h1 id="License">License</h1>

<p>This program is free software; you can redistribute it and/or modify it
under the terms of the <a href="https://www.gnu.org/copyleft/gpl.html">GNU
General Public License</a> as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.</p>

<p>This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.</p>

<p>You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA.</p>

</body>
</html>
